import json

## Compare schedule on both local json file and url is different or not.
## If data of title and start time from local and url data are equal,
## then get verified as True.
def get_file_obj(file: str):
    text = open(file, 'r', encoding="utf-8").read()
    return json.loads(text)

obj_local_day_one = get_file_obj("local_day_one.json")
obj_local_day_two = get_file_obj("local_day_two.json")
obj_url_day_one = get_file_obj("url_day_one.json")
obj_url_day_two = get_file_obj("url_day_two.json")


def build_array(dump_list: list, obj):
    for o in obj:
        data = {}
        data["title"] = o["title"]
        data["start"] = o["start"]
        dump_list.append(data)
    return dump_list

local_day_one = build_array([], obj_local_day_one)
local_day_two = build_array([], obj_local_day_two)
url_day_one = build_array([], obj_url_day_one)
url_day_two = build_array([], obj_url_day_two)

print(local_day_two)
print("If first day verified: " + str(local_day_one == url_day_one))
print("If second day verified: " + str(local_day_two == url_day_two))