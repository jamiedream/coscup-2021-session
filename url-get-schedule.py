import json, datetime, requests

## LOAD DATA
schedule_url = "https://coscup.org/2021/json/session.json"
get_url = requests.get(schedule_url)
content = get_url.text
## JSON
data = json.loads(content)
day_one = []
day_two = []

def append_data(which_room: str, return_list:list, session):
    daily_data = {}
    daily_data["room"] = which_room
    daily_data["start"] = session["start"]
    daily_data["end"] = session["end"]
    daily_data["title"] = session["zh"]["title"]
    daily_data["description"] = session["zh"]["description"]
    return_list.append(daily_data)

for session in data["sessions"]:
    room = session["room"]
    date = datetime.datetime.strptime(session["start"], '%Y-%m-%dT%H:%M:%S+08:00').strftime('%Y-%m-%d')
    if date == "2021-07-31" and session["room"] == "TR311":
        append_data("TR311", day_one, session)
    if date == "2021-08-01" and session["room"] == "TR309":
        append_data("TR309", day_two, session)
        

day_one = sorted(day_one, key=lambda daily_data: datetime.datetime.strptime(daily_data["start"], '%Y-%m-%dT%H:%M:%S+08:00'))
day_two = sorted(day_two, key=lambda daily_data: datetime.datetime.strptime(daily_data["start"], '%Y-%m-%dT%H:%M:%S+08:00'))
with open('url_day_one.json', 'w') as outfile:
    json.dump(day_one, outfile, ensure_ascii=False)
with open('url_day_two.json', 'w') as outfile:
    json.dump(day_two, outfile, ensure_ascii=False)