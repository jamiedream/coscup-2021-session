import json
import requests
import datetime

text = open("COSCUP_2021_Slido.json", 'r', encoding="utf-8").read()
obj = json.loads(text)
day_one = []
day_two = []

def get_slido_code(slido_url):
    url = "https://app.sli.do/api/v0.5/app/events?hash=" + \
        slido_url.replace("https://app.sli.do/event/", "")
    return requests.get(url).json()['code']

def append_data(which_room: str, return_list:list, session):
    daily_data = {}
    daily_data["room"] = which_room
    daily_data["start"] = session["start"]
    daily_data["end"] = session["end"]
    daily_data["title"] = session["session_title"]
    daily_data["slido_url"] = session["slido_url"]
    daily_data["slido_url_shorten"] = get_slido_code(session["slido_url"])
    daily_data["track"] = session["track"]
    daily_data["speakers"] = session["speakers"]  
    daily_data["tags"] = session["tags"]  
    return_list.append(daily_data)

for session in obj:
    ##print('Processing', session["track"], session["session_title"])
    room = session["room"]
    date = datetime.datetime.strptime(session["start"], '%Y-%m-%dT%H:%M:%S+08:00').strftime('%Y-%m-%d')
    if date == "2021-07-31" and session["room"] == "TR311 Special Track":
        append_data("TR311", day_one, session)

    if date == "2021-08-01" and session["room"] == "TR309":
        append_data("TR309", day_two, session)

day_one = sorted(day_one, key=lambda daily_data: datetime.datetime.strptime(daily_data["start"], '%Y-%m-%dT%H:%M:%S+08:00'))
day_two = sorted(day_two, key=lambda daily_data: datetime.datetime.strptime(daily_data["start"], '%Y-%m-%dT%H:%M:%S+08:00'))
with open('local_day_one.json', 'w') as outfile:
    json.dump(day_one, outfile, ensure_ascii=False)
with open('local_day_two.json', 'w') as outfile:
    json.dump(day_two, outfile, ensure_ascii=False)